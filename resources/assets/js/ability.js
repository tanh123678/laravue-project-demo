import { AbilityBuilder } from 'casl/ability'
// import axios from 'axios';
// let ability = new AbilityBuilder();

const casl = require("casl/ability");

export default function defineRulesFor(role,permission) {
          return casl.AbilityBuilder.define(can => {
          console.log(permission);
          can(permission, 'all');
       }
   );
}; 
