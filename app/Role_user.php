<?php

namespace App;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\Role_user as Authenticatable;
class Role_user extends Model
{
    protected $fillable = [
        'user_id', 'role_id'
    ];
}
